#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>

#define CHILD_PROCESS 0

#define FORK_ERROR -1
#define WAIT_ERROR -1

#define IDX_OF_FILENAME 1

#define EXPECTED_AMOUNT_OF_ARGUMENTS 2

int main(int argc, char ** argv) {

    if (argc != EXPECTED_AMOUNT_OF_ARGUMENTS) {
        fprintf(stderr, "Error: expecting one argument (name of a long file)\n");
        return EXIT_FAILURE;
    }

    pid_t pid = fork();
    
    if (pid == FORK_ERROR) {
        perror("Error while forking process");
        return EXIT_FAILURE;
    }

    if (pid == CHILD_PROCESS) {
        execl("/bin/cat", "cat", argv[IDX_OF_FILENAME], NULL);
        perror("Failed to execute cat");
        return EXIT_FAILURE;
    }

    pid_t waitResult = wait(NULL);
    if (waitResult == WAIT_ERROR) {
        perror("Wait error: ");
        return EXIT_FAILURE;
    }

    printf("Output from parent process\n");

    return EXIT_SUCCESS;
}
