#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>

#define FORK_ERROR -1
#define WAIT_ERROR -1
#define CHILD_ID 0
#define MIN_AMOUNT_OF_ARGUMENTS 2

extern char ** environ;

int execvpe(const char * filename, char * argv[], char * envp[]) {
    char ** old_environ = environ;
    environ = envp;
    execvp(filename, argv);
    environ = old_environ;

    return EXIT_FAILURE;
}

int main(int argc, char* argv[]) {
    if (argc < MIN_AMOUNT_OF_ARGUMENTS) {
        fprintf(stderr, "Expecting at least one argument: name of program\n");
        return EXIT_FAILURE;
    }


    char * command = argv[1];
    char ** command_args = &argv[1];
    char * new_envp[] = {"ENV1 = VAL1", "ENV2 = VAL2", "ENV3=VAL3", NULL};

    pid_t process_id;
    process_id = fork();

    if (process_id == FORK_ERROR) {
        perror("Could not fork the process");
        return EXIT_FAILURE;
    }

    if (process_id == CHILD_ID) {
        execvpe(command, command_args, new_envp);
        perror("Could not execute command");
        
        return EXIT_FAILURE;
    }


    int status;
    pid_t wait_child = wait(&status);

    if (wait_child == WAIT_ERROR) {
        perror("Error while waiting for process to terminate");
         
        return EXIT_FAILURE;
    }


    if(WIFEXITED(status)) {
        int exit_status = WEXITSTATUS(status);
        printf("Process exit with exit status %d\n", exit_status);
        
        return EXIT_SUCCESS;
    }
    
    if(WIFSIGNALED(status)) {
        int signal_num = WTERMSIG(status);
        printf("Process terminated by signal %d\n", signal_num);
        
        return EXIT_SUCCESS;
    }

    perror("Process terminated for an unknown reason");
    
    return EXIT_FAILURE;
}