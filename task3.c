#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

int main(int argc, char ** argv) {

    char * fileName = argc > 1 ? argv[1] : "file";

    uid_t real_uid = getuid();
    uid_t effective_uid = geteuid();

    int error;

    printf("Real UID = %d\n", real_uid);
    printf("Effective UID = %d\n", effective_uid);

    FILE * file = fopen(fileName, "rw");
    if (file == NULL) {
        perror("Failed to open file");
        return -1;
    }
    printf("Successfully opened file\n");

    error = fclose(file);
    if (error != 0) {
        perror("Failed to close the file");
        return -1;
    }
    printf("Successfully closed file\n");

    error = setuid(real_uid);
    if (error != 0) {
        perror("setuid() failed");
        return -1;
    }

    printf("Real UID = %d\n", getuid());
    printf("Effective UID = %d\n", geteuid());

    file = fopen(fileName, "rw");
    if (file == NULL) {
        perror("Failed to open file");
        return -1;
    }
    printf("Successfully opened file\n");

    error = fclose(file);
    if (error != 0) {
        perror("Failed to close the file");
        return -1;
    }
    printf("Successfully closed file\n");

    return 0;
}