#include <iostream>
#include <cstring>

#define MAX 256

using namespace std;

struct List{
    char * string;
    List * next;
};

void deleteOne(List * node)
{
    free(node->string);
    free(node);
}

void deleteList(List * head)
{
    List * temp, * cur;

    for (temp = head; temp->next != NULL; temp = cur)
    {
        cur = temp->next;
        deleteOne(temp);
    }

    deleteOne(temp);
}

int createNode(List ** node, char * string)
{
    * node = (List*)malloc(sizeof(List));
    if (* node == NULL)
    {
        perror("Memory allocation failure in createNode");
        return EXIT_FAILURE;
    }

    (* node)->next = NULL;
    (* node)->string = string;
    return EXIT_SUCCESS;
}

int addNode(List ** head, char * string)
{
    int error;
    List * temp, * last;
    error = createNode(&temp, string);
    if (error == EXIT_FAILURE)
        return EXIT_FAILURE;

    if(* head == NULL)
    {
        * head = temp;
        return EXIT_SUCCESS;
    }

    for(last = * head; last->next != NULL; last = last->next);
    last->next = temp;

    return EXIT_SUCCESS;
}

int getString(char ** string)
{
    char buf[MAX+1] = {0};
    char * error, * temp;
    char * line = NULL;
    int length = 1;

    for (int i = 1; buf[length - 1] != '\n'; i++)
    {
        error = fgets(buf, MAX, stdin);
        if (error == NULL)
        {
            perror("Failure fgets in get_str");
            free(line);
            return EXIT_FAILURE;
        }

        length = strlen(buf);
        temp = (char*)realloc(line, MAX*i + 1);
        if(temp == NULL)
        {
            perror("Memory allocation failure in get_str");
            free(line);
            return EXIT_FAILURE;
        }

        line = temp;
        strcat(line, buf);
    }

    * string = temp;
    return EXIT_SUCCESS;
}

int getList (List ** head)
{
    int error;
    char * string;

    do
    {
        error = getString(&string);
        if (error == EXIT_FAILURE)
        {
            return EXIT_FAILURE;
        }
        
        error = addNode(head, string);
        if(error == EXIT_FAILURE)
        {
            return EXIT_FAILURE;
        }
    } while (str[0] != '.');
    
    return EXIT_SUCCESS;
}

void printList(List * head)
{
    List * cur;
    for(cur = head; cur->next != NULL; cur = cur->next)
    {
        printf("%s", cur->string);
    }
}

int main(int argc, char * argv[])
{
    List * head = NULL;
    int error;

    error  = getList(&head);
    if (error == EXIT_FAILURE)
    {
        return EXIT_FAILURE;
    }
    printList(head);
    deleteList(head);
    return EXIT_SUCCESS;
}